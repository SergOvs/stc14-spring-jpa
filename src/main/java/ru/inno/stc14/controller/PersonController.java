package ru.inno.stc14.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.stc14.entity.dto.PersonDTO;
import ru.inno.stc14.service.PersonService;

import java.text.SimpleDateFormat;

@Controller
@RequestMapping("/person")
public class PersonController {

    private static final String REDIRECT_PERSON_LIST = "redirect:/person/list";
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    private final PersonService service;

    public PersonController(PersonService service) {
        this.service = service;
    }

    @RequestMapping("/list")
    public String getList(Model model) {
        model.addAttribute("persons", service.getList());
        return "person/list";
    }

    @RequestMapping("/get/{id}")
    public String getDetail(
            @PathVariable Long id,
            Model model) {
        PersonDTO person = service.getPerson(id);
        model.addAttribute("person", person);
        if (person.getBirthDate() != null) {
            model.addAttribute("birthDate", formatter.format(person.getBirthDate()));
        }
        return "person/detail";
    }

    @PostMapping("/add")
    public String add(@RequestParam String name,
                      @RequestParam String birthDate,
                      Model model) {
        service.create(name, birthDate);
        return REDIRECT_PERSON_LIST;
    }

    @PostMapping("/update")
    public String update(
            @RequestParam Long id,
            @RequestParam String name,
            @RequestParam String birthDate,
            Model model) {
        service.update(id, name, birthDate);
        return REDIRECT_PERSON_LIST;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id,
                         Model model) {
        service.delete(id);
        return REDIRECT_PERSON_LIST;
    }
}
