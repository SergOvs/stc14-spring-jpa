package ru.inno.stc14.entity.dto;

import ru.inno.stc14.entity.Person;

import java.util.Date;

public class PersonDTO {
    private Long id;
    private String name;
    private Date birthDate;

    public PersonDTO() {
    }

    public PersonDTO(Long id, String name, Date birthDate) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
    }

    public PersonDTO(Person person) {
        this.id = person.getId();
        this.name = person.getName();
        this.birthDate = person.getBirthDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
