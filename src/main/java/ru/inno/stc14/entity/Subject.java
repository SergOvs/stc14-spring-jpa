package ru.inno.stc14.entity;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class Subject {
    private UUID id;
    private String description;
    private List<Person> personList;

    @Id
    @GeneratedValue
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany
    @JoinTable(name = "Course")
    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }
}
