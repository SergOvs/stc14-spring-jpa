package ru.inno.stc14.repository;

import ru.inno.stc14.entity.Person;

import java.util.List;

public interface PersonDAO {

    void create(Person person);

    void update(Person person);

    void delete(Person person);

    List<Person> getList();

    Person getPerson(Long id);
}
