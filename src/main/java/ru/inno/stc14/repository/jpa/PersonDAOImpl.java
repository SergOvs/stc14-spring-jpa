package ru.inno.stc14.repository.jpa;

import org.springframework.stereotype.Repository;
import ru.inno.stc14.entity.Person;
import ru.inno.stc14.repository.PersonDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class PersonDAOImpl implements PersonDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Person person) {
        entityManager.merge(person);
    }

    @Override
    public void update(Person person) {
        entityManager.persist(person);
    }

    @Override
    public void delete(Person person) {
        entityManager.remove(person);
    }

    @Override
    public List<Person> getList() {
        TypedQuery<Person> query =
                entityManager.createQuery(
                        "from Person",
                        Person.class);
        return query.getResultList();
    }

    @Override
    public Person getPerson(Long id) {
        return entityManager.find(Person.class, id);
    }
}
