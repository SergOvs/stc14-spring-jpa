package ru.inno.stc14.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.inno.stc14.entity.Person;
import ru.inno.stc14.entity.dto.PersonDTO;
import ru.inno.stc14.repository.PersonDAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    private final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    private final PersonDAO personDAO;

    @Autowired
    public PersonServiceImpl(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    @Override
    public void create(String name, String birthDate) {
        Date date = safeParseDate(birthDate);
        Person person = new Person(name, date);

        personDAO.create(person);
    }

    private Date safeParseDate(String birthDate) {
        Date date = null;
        try {
            date = formatter.parse(birthDate);
        } catch (ParseException e) {
            LOGGER.warn(e.getMessage());
        }
        return date;
    }

    @Override
    public void update(Long id, String name, String birthDate) {
        Person person = personDAO.getPerson(id);
        if (person == null) {
            return;
        }
        Date date = safeParseDate(birthDate);
        person.setName(name);
        person.setBirthDate(date);
        personDAO.update(person);
    }

    @Override
    public void delete(Long id) {
        Person person = personDAO.getPerson(id);
        personDAO.delete(person);
    }

    @Override
    public List<PersonDTO> getList() {
        List<Person> dao = personDAO.getList();

        return dao.stream()
                .map(PersonDTO::new)
                .collect(Collectors.toList());
    }

    @Override
    public PersonDTO getPerson(Long id) {
        Person entity = personDAO.getPerson(id);
        PersonDTO dto = new PersonDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }
}
