package ru.inno.stc14.service;

import ru.inno.stc14.entity.dto.PersonDTO;

import java.util.List;

public interface PersonService {

    void create(String name, String birthDate);

    void update(Long id, String name, String birthDate);

    void delete(Long id);

    List<PersonDTO> getList();

    PersonDTO getPerson(Long id);

}
